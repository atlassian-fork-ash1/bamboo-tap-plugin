package com.atlassian.bamboo.plugins.tap;

import java.io.File;
import java.util.Set;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestReportCollector;

import com.google.common.collect.Sets;

import org.jetbrains.annotations.NotNull;

public class TapReportCollector implements TestReportCollector {
    private final boolean failFailedTodoResults;

    public TapReportCollector(boolean failFailedTodoResults) {
        this.failFailedTodoResults = failFailedTodoResults;
    }

    @NotNull
    @Override
    public TestCollectionResult collect(@NotNull File file) throws Exception {
        return new TapTestReportProvider(file, failFailedTodoResults).getTestCollectionResult();
    }

    @NotNull
    @Override
    public Set<String> getSupportedFileExtensions() {
        return Sets.newHashSet();
    }
}
