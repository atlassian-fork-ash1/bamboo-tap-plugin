1..5
ok 1 MyModule method1() doesn't wrap array input in a second array
not ok 2 MyModule method1() wraps non-array input in an array
  AssertionError: expected [ 'abc' ] to equal [ 'abcd' ]
      at Object.Assertion.eql (/Volumes/SSD/src/my-module/node_modules/should/lib/should.js:285:10)
      at Context.<anonymous> (/Volumes/SSD/src/my-module/test/my-module-test.js:33:47)
      at Test.Runnable.run (/Volumes/SSD/src/my-module/node_modules/mocha/lib/runnable.js:211:32)
      at Runner.runTest (/Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:352:10)
      at /Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:398:12
      at next (/Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:278:14)
      at /Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:287:7
      at next (/Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:234:23)
      at Object._onImmediate (/Volumes/SSD/src/my-module/node_modules/mocha/lib/runner.js:255:5)
      at processImmediate [as _immediateCallback] (timers.js:330:15)
ok 3 MyModule method2() pads values with leading zeroes # SKIP -
not ok 4 MyModule method2() doesn't truncate numbers bigger than the supplied size # some sort of failure message here
ok 5 MyModule method3() doesn't touch a normal date-as-string
# tests 4
# pass 2
# fail 2
